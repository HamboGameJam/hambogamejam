﻿using UnityEngine;
using System.Collections;

public class CBulletLogic : MonoBehaviour
{
	public float m_fForwardSpeed;
	public CStateMachine m_stateMachine;
	public IGun m_gun;

	

	void Awake()
	{
		m_fForwardSpeed = 300.00f;

		m_stateMachine =
			gameObject.GetComponent<CStateMachine>();

		CState stateRecycled = m_stateMachine.StateInsert( "Recycled" );
		CState stateSpawn    = m_stateMachine.StateInsert( "Spawn" );
		CState stateUpdate   = m_stateMachine.StateInsert( "Update" );
		CState stateDying    = m_stateMachine.StateInsert( "Dying" );


		CBulletRecycledStateBehaviour bulletRecycledStateBehaviour =
			gameObject.AddComponent("CBulletRecycledStateBehaviour") as CBulletRecycledStateBehaviour;
		( (CStateBehaviour)bulletRecycledStateBehaviour ).stateMachineParent = m_stateMachine;
		bulletRecycledStateBehaviour.m_bulletLogic = this;

		CBulletSpawnStateBehaviour bulletSpawnStateBehaviour =
			gameObject.AddComponent("CBulletSpawnStateBehaviour") as CBulletSpawnStateBehaviour;
		( (CStateBehaviour)bulletSpawnStateBehaviour ).stateMachineParent = m_stateMachine;
		bulletSpawnStateBehaviour.m_bulletLogic = this;
		
		CBulletUpdateStateBehaviour bulletUpdateStateBehaviour =
			gameObject.AddComponent("CBulletUpdateStateBehaviour") as CBulletUpdateStateBehaviour;
		( (CStateBehaviour)bulletUpdateStateBehaviour ).stateMachineParent = m_stateMachine;
		bulletUpdateStateBehaviour.m_bulletLogic = this;
		
		CBulletDyingStateBehaviour bulletDyingStateBehaviour =
			gameObject.AddComponent("CBulletDyingStateBehaviour") as CBulletDyingStateBehaviour;
		( (CStateBehaviour)bulletDyingStateBehaviour ).stateMachineParent = m_stateMachine;
		bulletDyingStateBehaviour.m_bulletLogic = this;

		stateRecycled.ScriptInsert( bulletRecycledStateBehaviour );
		stateSpawn.ScriptInsert( bulletSpawnStateBehaviour );
		stateUpdate.ScriptInsert( bulletUpdateStateBehaviour );
		stateDying.ScriptInsert( bulletDyingStateBehaviour );

		m_stateMachine.stateActive = "Recycled";
		//Debug.Log("m_stateMachine.stateActive = Spawn;");
		//Debug.Break();
	}
	

}
