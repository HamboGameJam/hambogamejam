﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CGameplayInitStateBehaviour : CStateBehaviour
{
	public CSharedData  m_sharedData;

	override public void OnEnabled()
	{
		this.enabled = true;
		Debug.Log("CGameplayInitStateBehaviour:OnEnabled");
		
		Initialize();
		
		m_statemachineParentReference.stateActive = "Update";
	}

	override public void OnDisabled()
	{
		this.enabled = false;
		Debug.Log("CGameplayInitStateBehaviour:OnDisabled");
	}

	private void Initialize_GetSpawnPoints()
	{
		this.m_sharedData.arrGameObjectSpawnPoints =
			GameObject.FindGameObjectsWithTag( "Respawn" );

		//Debug.Break ();
	}

	private GameObject Initialize_SpawnPlayer( GameObject gameObjectSpawnPoint,
	                                           string strPlayerId               )
	{
		GameObject gameObjectPlayer = 
			GameObject.Instantiate( Resources.Load("Prefabs/prefab"+strPlayerId) ) as GameObject;

		gameObjectPlayer.GetComponent<CPlayerLogic>().id = strPlayerId;
		gameObjectPlayer.GetComponent<CPlayerLogic>().m_fHealth = 1.00f;

		gameObjectPlayer.name = "gameObjectPlayer"+strPlayerId;
		Vector3 v3LocalPosition = new Vector3( gameObjectSpawnPoint.transform.localPosition.x,
		                                       gameObjectSpawnPoint.transform.localPosition.y,
		                                       gameObjectSpawnPoint.transform.localPosition.z  );

		gameObjectPlayer.transform.localPosition = v3LocalPosition;


		return gameObjectPlayer;
	}

	private void Initialize_SpawnPlayers()
	{
		this.m_sharedData.arrGameObjectPlayers =
			new GameObject[4];	
		this.m_sharedData.arrGameObjectPlayers[0] = 
			Initialize_SpawnPlayer( this.m_sharedData.arrGameObjectSpawnPoints[ 0 ], "P0" );

		this.m_sharedData.arrGameObjectPlayers[1] = 
			Initialize_SpawnPlayer( this.m_sharedData.arrGameObjectSpawnPoints[ 1 ], "P1" );
		
		this.m_sharedData.arrGameObjectPlayers[2] = 
			Initialize_SpawnPlayer( this.m_sharedData.arrGameObjectSpawnPoints[ 2 ], "P2" );
		
		this.m_sharedData.arrGameObjectPlayers[3] = 
			Initialize_SpawnPlayer( this.m_sharedData.arrGameObjectSpawnPoints[ 3 ], "P3" );
	}


	private void Initialize_SpawnRecycledBullets()
	{
		int iTotalNumberOfBullets = 300;
		GameObject gameObjectInsantiatedBullet;

 		//this.m_sharedData.listGameObjectRecycledBullets = new List<GameObject>();
		//this.m_sharedData.listGameObjectActiveBullets   = new List<GameObject>();

		this.m_sharedData.stackGameObjectRecycledBullets = new Stack<GameObject>();
		this.m_sharedData.stackGameObjectActiveBullets   = new Stack<GameObject>();

		GameObject bulletParent = new GameObject("Bullet Parent");
		GameObject bulletPrefab = (GameObject)Resources.Load("Prefabs/prefabBullet");

		for( int iBulletIndex = 0;
		    	 iBulletIndex < iTotalNumberOfBullets;
		    	 iBulletIndex++                        )
		{
			Debug.Log("494949494949494949 Initialize_SpawnRecycledBullets");
			gameObjectInsantiatedBullet = GameObject.Instantiate(bulletPrefab) as GameObject;
			//gameObjectInsantiatedBullet.SetActive( false );

			gameObjectInsantiatedBullet.transform.parent = bulletParent.transform;

			//gameObjectInsantiatedBullet.GetComponent<CBulletLogic>().Initialize(); 

			this.m_sharedData.stackGameObjectRecycledBullets.Push( gameObjectInsantiatedBullet );
		}
	}

	private void Initialize()
	{
		Initialize_GetSpawnPoints();
		Initialize_SpawnPlayers();
		Initialize_SpawnRecycledBullets();
		/*

		InitializeSpawnPointsShuffle(); 
		InitializeSpawnPointsSetPlayers();
		InitializeAddFollowCamerasToPlayers();
		InitializeGUICamera();
		InitializeStorePlayersGUI();
		*/
	}
}
