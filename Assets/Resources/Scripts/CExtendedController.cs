﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Physics/Extended Controller")]
public class CExtendedController : MonoBehaviour
{
	void Awake()
	{
		m_v3SpeedCurrent                 = new Vector2( 00.00f, 00.00f );
		m_v3CurrentDirection             = gameObject.transform.forward.normalized;

		m_fThrusterSpeed                 = 5.00f;
		m_fRotationSpeed                 = 1.00f;
		m_characterController            = gameObject.GetComponent<CharacterController>();	
	}
	

	void FixedUpdate()
	{
		Vector3 v3ThisFrameMovement; //

		m_characterController.rigidbody.velocity =
			transform.InverseTransformDirection(m_characterController.rigidbody.velocity); //objectspeed in local space

		//m_collisionFlagsLast = m_characterController.Move(v3ThisFrameMovement);

		//	gameObject.transform.forward
		//rigidbody.velocity = transform.forward * speed;
	}

	public CharacterController m_characterController;
	public CollisionFlags      m_collisionFlagsLast;
	public float               m_fThrusterSpeed;
	public float               m_fRotationSpeed;

	public Vector3             m_v3CurrentDirection;
	public Vector3             m_v3SpeedCurrent;

}
