﻿using UnityEngine;
using System.Collections;

public class CPlayerPlayingStateBehaviour : CStateBehaviour
{
	public CPlayerLogic m_playerLogic;
	Vector3 v3ForceTotal;

	void Awake()
	{
		v3ForceTotal = new Vector3();
	}

	override public void OnEnabled()
	{
		this.enabled = true;
	}
	
	override public void OnDisabled()
	{
		this.enabled = false;
	}

	private void Update_ProcessInput()
	{
		Update_ProcessInput_GunButton();
		Update_ProcessInput_BombButton();
		Update_ProcessInput_AxisHorizontal();
		Update_ProcessInput_ThrusterButton();
		//Update_ProcessInput_AxisVertical();		
		
	}

	private void Update_ProcessInput_ThrusterButton()
	{
		if( Input.GetButton( ""+m_playerLogic.id+"_THRUST" ) )
		{
			Debug.Log( "THRUST "+m_playerLogic.id );
			
			//gameObject.rigidbody.AddForce( new Vector3( 0.00f, 0.00f, 1.00f) );
			v3ForceTotal.x = gameObject.transform.forward.x * m_playerLogic.m_fThrusterSpeed;
			v3ForceTotal.y = gameObject.transform.forward.y * m_playerLogic.m_fThrusterSpeed;
			v3ForceTotal.z = gameObject.transform.forward.z * m_playerLogic.m_fThrusterSpeed;

			//gameObject.rigidbody.AddForce( gameObject.transform.forward );
			gameObject.rigidbody.AddForce( v3ForceTotal );
		}
	}

	private void Update_ProcessInput_GunButton()
	{
		if( Input.GetButton( ""+m_playerLogic.id+"_GUN" ) )
		{
			Debug.Log( "GUN "+m_playerLogic.id );

			m_playerLogic.m_gunManager.Shoot();



		}
	}

	private void Update_ProcessInput_BombButton()
	{
		if( Input.GetButton( ""+m_playerLogic.id+"_BOMB" ) )
		{
			Debug.Log( "BOMB "+m_playerLogic.id );
		}
	}

	private void Update_ProcessInput_AxisHorizontal()
	{
		float fInputAxisHorizontal = Input.GetAxis( ""+m_playerLogic.id+"_HORIZONTAL" );
		if( fInputAxisHorizontal != 0.00f )
		{
			Debug.Log( "HORIZONTAL "+m_playerLogic.id+" " );

			transform.RotateAround(    gameObject.transform.position,
			                           Vector3.up,
			                         ( m_playerLogic.m_fRotationSpeed * fInputAxisHorizontal )  );
		}

	}
	
	void Update()
	{
		Update_ProcessInput();
	}
}
