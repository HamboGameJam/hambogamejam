﻿using UnityEngine;
using System.Collections;
public class EdgeMagic : MonoBehaviour {
	public enum EdgeAxis {x,y,z};
	public EdgeAxis edgeAxis;
	public float reapearNumber;
	void OnTriggerEnter(Collider col){
		if(edgeAxis == EdgeAxis.x){
			Vector3 temp = 
				new Vector3(reapearNumber,col.gameObject.transform.position.y,col.gameObject.transform.position.z);
			col.gameObject.transform.position = temp;
		}
		else if(edgeAxis == EdgeAxis.y){
			Vector3 temp = 
				new Vector3(col.gameObject.transform.position.x,reapearNumber,col.gameObject.transform.position.z);
			col.gameObject.transform.position = temp;
		}
		else if(edgeAxis == EdgeAxis.z){
			Vector3 temp = 
				new Vector3(col.gameObject.transform.position.x,col.gameObject.transform.position.y,reapearNumber);
			col.gameObject.transform.position = temp;
		}
	}
}
