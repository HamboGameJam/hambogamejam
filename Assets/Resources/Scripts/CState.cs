﻿using UnityEngine;
using System.Collections;

public class CState : MonoBehaviour
{
	[SerializeField]
	private string m_strID;
	
	public string StateID
	{
		get
		{
			return m_strID;
		}
		
		set
		{
			m_strID = string.Copy( value );
		}
		
	}
	
	
	public void OnEnable()
	{
		if( m_lstScripts.Count > 0 )
		{   
			foreach( CStateBehaviour mbScript in m_lstScripts )
			{
				mbScript.enabled = true;
				mbScript.OnEnabled();
			}
		}
		
	}
	
	public void OnDisable()
	{
		if( m_lstScripts.Count > 0 )
		{
			foreach( CStateBehaviour mbScript in m_lstScripts )
			{
				mbScript.OnDisabled();
				mbScript.enabled = false;
			}
		}
		
	}
	
	public void ScriptRemove( CStateBehaviour mbScriptInstance )
	{
		if( mbScriptInstance != null )
		{
			CStateBehaviour mbScriptCompare;
			
			int iCurrentScriptIndex;
			
			for( iCurrentScriptIndex = 0;
			     iCurrentScriptIndex < m_lstScripts.Count;
			     iCurrentScriptIndex ++                    )
			{ 
				mbScriptCompare = ( CStateBehaviour )m_lstScripts[ iCurrentScriptIndex ];
				
				if( mbScriptInstance == mbScriptCompare )
				{ 
					ScriptRemove( iCurrentScriptIndex );
					break;
				}
			}
		}
		
	}
	
	private void ScriptRemove(int iScriptIndex)
	{
		CStateBehaviour objScript = ( CStateBehaviour )m_lstScripts[ iScriptIndex ];
		CStateBehaviour.Destroy( objScript );
		m_lstScripts.RemoveAt( iScriptIndex );
		
	}
	
	public MonoBehaviour ScriptInsert( CStateBehaviour mbScriptInsert )
	{
		CStateBehaviour mbScriptReturn = null;
		
		foreach( CStateBehaviour mbehaviour in m_lstScripts )
		{
			if( mbScriptInsert.GetInstanceID() == mbehaviour.GetInstanceID() )
			{
				return mbScriptInsert;
			}
		}
		
		mbScriptReturn = mbScriptInsert;
		m_lstScripts.Add( mbScriptInsert );
		mbScriptInsert.enabled = false;
		
		return mbScriptReturn;
		
	}
	
	public System.Collections.ArrayList m_lstScripts = new System.Collections.ArrayList();	
}
