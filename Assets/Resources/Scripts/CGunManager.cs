﻿using UnityEngine;
using System.Collections;

public class CGunManager : MonoBehaviour
{
	[SerializeField] private IGun m_gunObject;

	public void Shoot()
	{
		m_gunObject.Shoot();
	}


}
