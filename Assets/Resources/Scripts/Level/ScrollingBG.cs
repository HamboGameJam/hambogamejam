using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ScrollingBG : MonoBehaviour
{
	[SerializeField] private Renderer bgRenderer;
	[SerializeField] private float scrollSpeedX;
	[SerializeField] private float scrollSpeedY;

	private Vector2 offset;

	void Update() {
		offset.x += scrollSpeedX * Time.deltaTime;
		offset.y += scrollSpeedY * Time.deltaTime;
		bgRenderer.material.mainTextureOffset = offset;
	}

}
