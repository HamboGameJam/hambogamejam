//11 -1    -14
using UnityEngine;
using System.Collections;

public class CStateMachine : MonoBehaviour
{
	private CState statePlaceholder;
	
	void Start()
	{	
		m_fElpasedTimeStateCurrent  = 0.00f;
		m_fElapsedTimeStatePrevious = 0.00f;
		
	}
	
	void Update ()
	{	
		m_fElpasedTimeStateCurrent  += Time.deltaTime;
		
	}
	
	public float previousStateElapsedTime
	{
		get{ return m_fElapsedTimeStatePrevious;  }
		set{ m_fElapsedTimeStatePrevious = value; }
	}
	
	public float currentStateElapsedTime
	{
		get{ return m_fElpasedTimeStateCurrent;  }
		set{ m_fElpasedTimeStateCurrent = value; }
	}
	
	public string stateActive
	{
		get
		{
			string strStateName = null;
			
			if (m_lstStates.Count > 0)
			{
				CState state = (CState)m_lstStates[m_iActiveStateIndex];
				strStateName = state.StateID;
			}
			
			return strStateName;
			
		}
		
		set
		{
			m_fElapsedTimeStatePrevious = m_fElpasedTimeStateCurrent;
			m_fElpasedTimeStateCurrent  = 0;
			
			CState state;
			
			for (int iCurrentStateIndex = 0;
			     iCurrentStateIndex < m_lstStates.Count;
			     iCurrentStateIndex++)
			{
				state = (CState)m_lstStates[iCurrentStateIndex];
				
				if (state.StateID == value)
				{
					int iPreviousStateIndex = m_iActiveStateIndex;
					m_iActiveStateIndex = iCurrentStateIndex;
					
					((CState)m_lstStates[iPreviousStateIndex]).OnDisable();
					((CState)m_lstStates[m_iActiveStateIndex]).OnEnable();
					
					
					break;
				}
			}
		}
		
	}
	
	/*
	public stringRandom
	{
		get
        {
        	
            string strStateName = null;

            if (m_lstStates.Count > 0)
            {
                State state = (State)m_lstStates[m_iActiveStateIndex];
                strStateName = state.StateID;
            }

            return strStateName;        
        }
        
	}
*/
	public CState StateInsert(string strStateName)
	{	
		CState stateReturn = null;
		CState stateExist;
		
		GameObject thisGameObject = gameObject;
		
		int iCurrentStateIndex;
		int iStringCompareResult;
		
		if( m_lstStates.Count > 0 )
		{	
			for ( iCurrentStateIndex = 0;
			     iCurrentStateIndex < m_lstStates.Count;
			     iCurrentStateIndex++                    )
			{       	  	
				stateExist = (CState)m_lstStates[iCurrentStateIndex];
				iStringCompareResult = string.Compare(strStateName, stateExist.StateID);
				
				if (iStringCompareResult < 0)
				{              		
					stateReturn = thisGameObject.AddComponent<CState>();
					stateReturn.StateID = string.Copy( strStateName );
					m_lstStates.Insert( iCurrentStateIndex, stateReturn );
					
					break;
				}
				else if(iStringCompareResult == 0)
				{
					stateReturn = stateExist;
				}
				else if(  (iStringCompareResult > 0                        ) &&
				        (iCurrentStateIndex   == (m_lstStates.Count - 1) )     )
				{
					stateReturn = thisGameObject.AddComponent<CState>();
					stateReturn.StateID = string.Copy( strStateName );
					
					m_lstStates.Add(stateReturn);
					
					break;
				}
			}
		}
		else
		{
			stateReturn = thisGameObject.AddComponent<CState>();
			stateReturn.StateID = string.Copy( strStateName );
			m_lstStates.Add(stateReturn);
		}
		
		return stateReturn;        
		
	}
	
	public void StateRemove(string strStateName)
	{
		CState state;
		int iStringCompareResult;
		
		for (int iCurrentStateIndex = 0;
		     iCurrentStateIndex < m_lstStates.Count;
		     iCurrentStateIndex++)
		{
			state = (CState)m_lstStates[iCurrentStateIndex];
			iStringCompareResult = string.Compare(strStateName, state.StateID);
			
			if (iStringCompareResult == 0)
			{
				state = (CState)m_lstStates[iCurrentStateIndex];
				m_lstStates.RemoveAt(iCurrentStateIndex);
				
				return;
			}
			else if (iStringCompareResult > 0)
			{
				return;
			}
		}
		
	}
	
	float m_fElpasedTimeStateCurrent;
	float m_fElapsedTimeStatePrevious;
	
	private int m_iActiveStateIndex;
	private System.Collections.ArrayList m_lstStates = new System.Collections.ArrayList();
}