﻿using UnityEngine;
using System.Collections;

public class CPlayerLogic : MonoBehaviour
{
	public string id;
	public float m_fHealth;
	public float m_fThrusterSpeed;
	public float m_fRotationSpeed;

	public CStateMachine m_stateMachine;
	public CGunManager   m_gunManager;

	void Start()
	{
		m_fHealth = 1.00f;
		m_fThrusterSpeed = 200.00f;
		m_fRotationSpeed = 5.00f;

		m_stateMachine =
			gameObject.GetComponent<CStateMachine>();

		CState stateIdle     = m_stateMachine.StateInsert( "Idle" );
		CState statePlaying  = m_stateMachine.StateInsert( "Playing" );
		CState stateDying    = m_stateMachine.StateInsert( "Dying" );

		CPlayerIdleStateBehaviour playerIdleStateBehaviour =
			gameObject.AddComponent("CPlayerIdleStateBehaviour") as CPlayerIdleStateBehaviour;
		( (CStateBehaviour)playerIdleStateBehaviour ).stateMachineParent = m_stateMachine;
		playerIdleStateBehaviour.m_playerLogic = this;

		CPlayerPlayingStateBehaviour playerPlayingStateBehaviour =
			gameObject.AddComponent("CPlayerPlayingStateBehaviour") as CPlayerPlayingStateBehaviour;
		( (CStateBehaviour)playerPlayingStateBehaviour ).stateMachineParent = m_stateMachine;
		playerPlayingStateBehaviour.m_playerLogic = this;
		
		CPlayerDyingStateBehaviour playerDyingStateBehaviour =
			gameObject.AddComponent("CPlayerDyingStateBehaviour") as CPlayerDyingStateBehaviour;
		( (CStateBehaviour)playerDyingStateBehaviour ).stateMachineParent = m_stateMachine;
		playerDyingStateBehaviour.m_playerLogic = this;


		stateIdle.ScriptInsert( playerIdleStateBehaviour );
		statePlaying.ScriptInsert( playerPlayingStateBehaviour );
		stateDying.ScriptInsert( playerDyingStateBehaviour );
		m_stateMachine.stateActive = "Playing";

		m_gunManager = gameObject.GetComponentInChildren<CGunManager>();
		//m_gunManager.m_playerLogic = this;
		//m_gunManager. = gameObject.AddComponent<CGunBasic>();

		/*

	if( Input.GetButton( ""+m_playablePaloma.id+"_Beak" ) )
		{
			m_statemachineParentReference.stateActive = "Beaking";
		} 		 */
	}
	
	// Update is called once per frame
	void Update()
	{
	
	}
}
