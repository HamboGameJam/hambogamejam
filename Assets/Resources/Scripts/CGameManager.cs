﻿using UnityEngine;
using System.Collections;

public class CGameManager : MonoBehaviour
{
	[HideInInspector] public CSharedData   m_sharedData;
	[HideInInspector] public CStateMachine m_stateMachine;

	void Awake()
	{
		AwakeInitializeSharedData();
		AwakeInitializeGuns(); 
		AwakeSetUpStates();	
	}

	private void AwakeInitializeGuns()
	{
		Debug.Log("THIS IS BEING CALLED");
		IGun.sharedData = m_sharedData;
	}

	private void AwakeInitializeSharedData()
	{
		this.m_sharedData = gameObject.AddComponent<CSharedData>();
	}

	private void AwakeSetUpStates()
	{
		m_stateMachine =
			gameObject.AddComponent<CStateMachine>();
		CState stateInit   = m_stateMachine.StateInsert( "Init"   );
		CState stateUpdate = m_stateMachine.StateInsert( "Update" );
		
		CGameplayInitStateBehaviour gameplayInitStateBehaviour =
			gameObject.AddComponent("CGameplayInitStateBehaviour") as CGameplayInitStateBehaviour;
		( (CStateBehaviour)gameplayInitStateBehaviour ).stateMachineParent = m_stateMachine;
		gameplayInitStateBehaviour.m_sharedData = this.m_sharedData;
		
		CGameplayUpdateStateBehaviour gameplayUpdateStateBehaviour =
			gameObject.AddComponent("CGameplayUpdateStateBehaviour") as CGameplayUpdateStateBehaviour;
		( (CStateBehaviour)gameplayUpdateStateBehaviour ).stateMachineParent = m_stateMachine;
		gameplayUpdateStateBehaviour.m_sharedData = this.m_sharedData;
		
		stateInit.ScriptInsert( gameplayInitStateBehaviour );		
		stateUpdate.ScriptInsert( gameplayUpdateStateBehaviour );
		
		m_stateMachine.stateActive = "Init";
	}
	
	
}
