﻿using UnityEngine;
using System.Collections;

public class CGameplayUpdateStateBehaviour : CStateBehaviour
{
	public CSharedData  m_sharedData;
	//public CPlayerLogic m_playerLogic;
	
	override public void OnEnabled()
	{
		this.enabled = true;
		Debug.Log("CGameplayUpdateStateBehaviour:OnEnabled");
		
	}
	
	override public void OnDisabled()
	{
		this.enabled = false;
		Debug.Log("CGameplayUpdateStateBehaviour:OnDisabled");
		
	}
	
	void Update()
	{
		//UpdatePlayersGUI();
	}
}
