using UnityEngine;
using System.Collections;

public abstract class CStateBehaviour : MonoBehaviour
{
	public CStateMachine stateMachineParent
	{
		get
		{
			return m_statemachineParentReference;
		}
		
		set
		{
			m_statemachineParentReference = value;
		}
		
	}

	//Not to be confused with OnEnable/OnDisable wich deals with object's scope rather than active/inactive state
	public abstract void OnEnabled();
	public abstract void OnDisabled();
	
	protected CStateMachine m_statemachineParentReference;
}
