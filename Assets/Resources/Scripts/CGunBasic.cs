﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CGunBasic : IGun
{
	private GameObject gameObjectBullet;
	private Vector3 m_v3PlaceholderPosition;
	private Vector3 m_v3PlaceholderRotation;
	private ObjectPool objectPoolBasicBullets;


	void Awake()
	{
		//m_v3PlaceholderPosition = new Vector3();
		//m_v3PlaceholderRotation
		objectPoolBasicBullets = gameObject.GetComponent<ObjectPool>();
	}

	public override void Shoot()
	{
		//Shoot_LoadBulletFromSharedData();
		Shoot_LoadBulletFromPool();
		Shoot_SetBulletRotation();
		Shoot_SetBulletPosition();
		Shoot_SetBulletParenting();
		Shoot_ShootBullet();
	}

	private void Shoot_SetBulletParenting()
	{

	}

	private void Shoot_SetBulletPosition()
	{
		m_v3PlaceholderPosition = new Vector3( gameObject.transform.position.x,
		                                       gameObject.transform.position.y,
		                                       gameObject.transform.position.z  );

		gameObjectBullet.transform.position =
			m_v3PlaceholderPosition;
	}

	//private void Shoot_LoadBulletFromSharedData_BulletSetRotation( GameObject gameObjectBullet )
	private void Shoot_SetBulletRotation()
	{
		m_v3PlaceholderRotation = new Vector3( gameObject.transform.eulerAngles.x,
		                                       gameObject.transform.eulerAngles.y,
		                                       gameObject.transform.eulerAngles.z  );

		m_v3PlaceholderRotation.x = gameObject.transform.eulerAngles.x;
		m_v3PlaceholderRotation.y = gameObject.transform.eulerAngles.y;
		m_v3PlaceholderRotation.z = gameObject.transform.eulerAngles.z;

		gameObjectBullet.transform.eulerAngles = m_v3PlaceholderRotation;
	}

	private void Shoot_LoadBulletFromPool()
	{
		Debug.Log("7373737373737 objectPoolBasicBullets == null"+(objectPoolBasicBullets == null) );

		gameObjectBullet = objectPoolBasicBullets.Available;
	}

	private void Shoot_ShootBullet()
	{
		Debug.Log("2626262626262 gameObjectBullet == null"+(gameObjectBullet == null) );
		Debug.Log("2626262626262 gameObjectBullet.GetComponent<CBulletLogic>() == null"+(gameObjectBullet.GetComponent<CBulletLogic>() == null) );
		Debug.Log("2626262626262 gameObjectBullet.GetComponent<CBulletLogic>().m_stateMachine == null"+( gameObjectBullet.GetComponent<CBulletLogic>().m_stateMachine == null) );
		gameObjectBullet.GetComponent<CBulletLogic>().m_stateMachine.stateActive = "Spawn";		
	}
	
}
