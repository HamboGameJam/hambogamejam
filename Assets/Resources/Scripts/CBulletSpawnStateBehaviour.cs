﻿using UnityEngine;
using System.Collections;

public class CBulletSpawnStateBehaviour : CStateBehaviour
{
	public CBulletLogic m_bulletLogic;
	private Vector3 v3ForceTotal;

	private void OnEnabled_Activated()
	{
		//m_bulletLogic.gameObject.SetActive (true);
		gameObject.SetActive(true);
	}

	private void OnEnabled_PlaySound()
	{
	}

	private void OnEnabled_StartMovement()
	{
		//m_bulletLogic.gameObject.agai
		/*
		Debug.Log("m_bulletLogic.gameObject.name "+
		           m_bulletLogic.gameObject.name+")"  );

		Debug.Log("m_bulletLogic.gameObject.transform.position("+
		          	 m_bulletLogic.gameObject.transform.position.x+","+
		             m_bulletLogic.gameObject.transform.position.y+","+
		          	 m_bulletLogic.gameObject.transform.position.z+")"  );
		*/
		/*
		v3ForceTotal.x = 
			m_bulletLogic.m_gun.m_gunManager.m_playerLogic.gameObject.transform.forward.x * m_bulletLogic.m_fForwardSpeed;

		v3ForceTotal.y = 
			m_bulletLogic.m_gun.m_gunManager.m_playerLogic.gameObject.transform.forward.y * m_bulletLogic.m_fForwardSpeed;

		v3ForceTotal.z = 
			m_bulletLogic.m_gun.m_gunManager.m_playerLogic.gameObject.transform.forward.z * m_bulletLogic.m_fForwardSpeed;


		Debug.Log("m_bulletLogic.gameObject.name "+
		          m_bulletLogic.gameObject.name+")"  );
		*/
		Debug.Log("789789789789 m_bulletLogic.gameObject.transform.position("+
		          m_bulletLogic.gameObject.transform.position.x+","+
		          m_bulletLogic.gameObject.transform.position.y+","+
		          m_bulletLogic.gameObject.transform.position.z+")"  );

		v3ForceTotal.x = 
			gameObject.transform.forward.x * m_bulletLogic.m_fForwardSpeed;
		
		v3ForceTotal.y = 
			gameObject.transform.forward.y * m_bulletLogic.m_fForwardSpeed;
		
		v3ForceTotal.z = 
			gameObject.transform.forward.z * m_bulletLogic.m_fForwardSpeed;

		gameObject.rigidbody.AddForce(v3ForceTotal);
	}

	override public void OnEnabled()
	{
		this.enabled = true;

		Debug.Log("9494949494949494949494949494949");

		OnEnabled_Activated();
		OnEnabled_PlaySound();
		OnEnabled_StartMovement();

		Debug.Log("38383838383838383838383838383838");

		stateMachineParent.stateActive = "Update";

	}
	
	override public void OnDisabled()
	{
		this.enabled = false;
	}

	// Use this for initialization
	void Start()
	{
		v3ForceTotal = new Vector3();
	}
	
	// Update is called once per frame
	void Update()
	{
		Debug.Log("bullet gameObject.transform.position("+
		           gameObject.transform.position.x+","+
		           gameObject.transform.position.y+","+
		           gameObject.transform.position.z+")"  );	
	}
}
